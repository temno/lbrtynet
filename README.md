LBRTYnet
========

LBRTYnet is a toolkit and framework for creating the infrastructure and methodology of a private, independent wide area network that runs parallel and is autonomous to what is called "the internet".

More than anything else, the goal with this project is to develop a network that is not managed / controlled by telecom and government.  A network that can function autonomously if the entire "internet" structure was taken down yet can intermingle with, as necessary or desirable, depending on the context.

Further, a goal is to ensure that everyone can host their own content with ease, and contribute to the knowledge and resources available to the general public.

This IS NOT a blockchain project.  This is an infrastructure project.  Blockchain and other distributed communication and function can and will be part of this network, but it is not the foundation of this project.

The LBRTYnet repository (this one) also has a wiki set up which will contain information on participating specifically in LBRTYnet:

https://gitlab.com/FRXglobal/lbrtynet/-/wikis/home

The following TLDs are currently in use in the LBRTYnet DNS root/authoritative server pool:

.fc -> Freedom Cell domains

.lbrty -> LBRTYnet domains

.off -> Open Food Forest

The opennic.org TLDs can be found here:

https://www.opennic.org/

For a little more information, visit the set up page at the FReedom eXchange website to get a glimpse of the network:

https://www.freedomexchange.ca/
